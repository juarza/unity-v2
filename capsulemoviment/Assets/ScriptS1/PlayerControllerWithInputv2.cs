﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv2: MonoBehaviour {

    public float speed;
    public float runSpeed;
    private float hInput, vInput;
    public bool controlmode;

	

	void Update () {
        // Get Input every frame
        hInput = Input.GetAxisRaw("Horizontal");//Left=-1,Center=0, Right=1(Discrete)
        vInput = Input.GetAxisRaw("Vertical");//Down=-1, Center=0, Up=1(Discrete)

        float step = speed;

        //Change speed if Run Button is pressed
        if (Input.GetButton("Run"))
        {
            step = runSpeed;

        }

        //Vertical Movement (Z axis)

        if (vInput!=0)
        {
            transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);


        }


        //Horizontal Movement (X axis)
        if (hInput != 0)
        {
            if (controlmode)
            {

                transform.Translate(hInput * Vector3.right * Time.deltaTime * step);
            }
            else
            {
                transform.Rotate(hInput * Vector3.right * Time.deltaTime * step*50);
            }


        }

    }
}
