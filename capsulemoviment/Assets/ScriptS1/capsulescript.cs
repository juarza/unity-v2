﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleMovement : MonoBehaviour {

    // Capsule speed when it´s moving
    public float speed = 5f;

    //use this for initialization
	void Start () {
        //Move fevi unity space units per second towards front direction (onli once)
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        Debug.Log("Moving From Start Event");

	}
	
	// Update is called once per frame
	void Update () {
        //Move five unity space units per second towards right direction(se ejequita por cada frame)
        transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        Debug.Log("Moving From Update Event");
		
	}
}
