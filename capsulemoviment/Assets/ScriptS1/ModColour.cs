﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModColour: MonoBehaviour {

    public Color Randomcolor;
    Renderer mirender;


    // Use this for initialization
    void Start () {

         mirender = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton ("Color")) 
        {
            Randomcolor = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
            mirender.material.SetColor("_Color", Randomcolor);
        }
        else
        {
            mirender.material.SetColor("_Color", Color.white);

            

        }

    }
}
