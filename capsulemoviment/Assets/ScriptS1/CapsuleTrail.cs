﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTrail : MonoBehaviour {

    private TrailRenderer trail;

    //With Range it possible to limmit the values that varibles can take 
    [Range(1, 50)]
    public float startWidth = 5;
    [Range(1, 50)]
    public float endWidth = 2;

    private void Start()
    {
        //Getting component´s reference present in the same GameObject
        if (!(trail = GetComponent<TrailRenderer>()))
        {
            Debug.LogError("No TrailRenderer component attached to the GameObject. ");
            enabled = false;
            return;
        }
    }	
	// Update is called once per frame
	void Update () {

        //Accessing trailRenderer component present in thesame GameObject

        trail.time = Random.Range(1, 100);
        trail.startWidth  = startWidth;
        trail.endWidth = endWidth;
   
    }
}
