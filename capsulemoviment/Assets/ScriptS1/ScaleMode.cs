﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleMode : MonoBehaviour {

    public Vector3 scale;

	// Use this for initialization
	void Start () {

        scale = transform.localScale;
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Scale"))
        {
            transform.localScale += new Vector3(Random.Range(0.5f,1),Random.Range(0.5f, 1),Random.Range(0.5f, 1));
        }
        else
        {
            transform.localScale = scale;
        }
		
	}
}
