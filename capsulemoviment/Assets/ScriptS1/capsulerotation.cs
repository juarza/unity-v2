﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsulerotation : MonoBehaviour {

    public float rotationDegrees = 5f;

	// Use this for initialization
	void Start () {
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotating Frrom Start Event");
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotating From Update Event");
		
	}
}
