﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public WaypointController waypointControl; //Reference to WaypointController Script
    public float speed = 5;
    private GameObject cam;

    private void Start()
    {
        cam = GameObject.Find("Main Camera");      //Get GameObject reference by name

    }





    // Update is called once per frame
    void Update () {
        float steep = speed * Time.deltaTime;//Unity units per second
        Vector3 currentWaypointPos = waypointControl.getCurrentWaypointPos();

        //Move player´s transform to currentWaypoint position
        transform.position = Vector3.MoveTowards(transform.position, currentWaypointPos, steep);

        //Move the camera and the player together
        cam.transform.position = transform.position - Vector3.forward * 10f;
        
    }
}
