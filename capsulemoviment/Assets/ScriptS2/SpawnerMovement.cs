﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour
{
    [Range(0.5f, 3f)] public float limit;
    public GameObject centro;
    float increment;
    public float Radious;
    public GameObject objectToSpawn;
    GameObject objAux;
    Renderer render;
    public Color color;

    private void Start()
    {
        render = GetComponent<Renderer>();
        increment = 0f;
    }


    void Update()
    {
        increment += Time.deltaTime;

        transform.Translate(Vector3.forward * Radious * Time.deltaTime);

        transform.RotateAround(centro.transform.position, Vector3.up, 20 * Time.deltaTime);

        render.material.SetColor("_Color", Color.red);


        if (increment >= limit)
        {
            increment = 0f;
            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject; 

            float scale = Random.Range(1, 10);                       
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(Vector3.right * 400, ForceMode.Impulse);

        }

    }
}
