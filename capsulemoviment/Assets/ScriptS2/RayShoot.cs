﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShoot : MonoBehaviour
{
Rigidbody proyectil; 
int  velocidadProyectil  = 100; 
float intervaloDisparos = 0.2f; 
int tamanoCargador = 10; 
private float  balasActuales; 
bool  recargando; 

private float disparoSiguiente = 0.0f; 

void Start()
    {
        balasActuales = tamanoCargador;
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > disparoSiguiente && balasActuales > 0 && !recargando){
            Disparar();
        }
        if (balasActuales == 0)
        {
            recargando = true;
        }
        if (recargando)
        {
            Recargar();
        }
        Debug.Log(balasActuales);

    }
    void Disparar()
    {
       
        disparoSiguiente = Time.time + intervaloDisparos;
        
        Rigidbody bala  = Instantiate(proyectil, transform.position, transform.rotation);
        
        bala.velocity = transform.TransformDirection(new Vector3 (0, 0, -velocidadProyectil));
        
        Physics.IgnoreCollision(bala.GetComponent < Collider > (), transform.root.GetComponent < Collider > ());
       
       

        balasActuales -= 1;
    }

    void Recargar()
    {
        if (balasActuales < tamanoCargador)
        {
            balasActuales += 1 * Time.deltaTime;
        }
        if (balasActuales == tamanoCargador)
        {
            recargando = false;
        }
    }

}